// 1. Define route components.
// These can be imported from other files
import Login from "@/components/Login";
import ChatRoom from "@/components/ChatRoom";
import {createRouter, createWebHistory} from "vue-router";
import NotFound from "@/components/NotFound";
import Home from "@/components/Home";


// 2. Define some routes
// Each route should map to a component.
// We'll talk about nested routes later.
const routes = [
  {path: '/', name:"Home", component: Home},
  {path: '/login', name: "Login", component: Login,},
  {path: '/chat', name: "ChatRoom", component: ChatRoom,},
  {path: '/404', component: NotFound},
  {path: "/:pathMatch(.*)*", redirect: '/404',},
];

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = createRouter({
  // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
  history: createWebHistory(),
  routes, // short for `routes: routes`
});

export default router;