const isNil = (obj) => {
    return obj === undefined || obj === null;
};

const _isNaN = (obj) => {
    return Number.isNaN(obj);
}

export {isNil, _isNaN};