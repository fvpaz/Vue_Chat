const getTypeOf = (type) => {
    var tag = '[object ' + type + ']';
    return function(obj) {
        return toString.call(obj) === tag;
    };
}

export default getTypeOf;