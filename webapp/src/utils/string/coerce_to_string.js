import {isNil, _isNaN} from "@/utils/object/is_nil_or_nan.mjs";

const coerceToString = (obj) => {
    if (isNil(obj) || _isNaN(obj))
        return '';

    return '' + obj;
};

export default coerceToString;