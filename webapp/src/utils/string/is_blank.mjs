import getTypeOf from "@/utils/object/get_typeof.mjs";
import coerce_to_string from "@/utils/string/coerce_to_string.js";

/**
 * @function isBlank
 * @description Check isBlank the String and test the regex -> `^[\s\xA0]+$`. Only use in `Strings`
 * @param obj The object to verify.
 * @returns {boolean} `true` if is empty, blank or [`undefined`, `isNaN`, `isNil`].
 */
const _isBlank = (obj) => {
    //let isString = getTypeOf('String');

    // if (!isString)
    const text = coerce_to_string(obj);

    return text.length === 0 || (/^[\s\xA0]+$/).test(text)
};

export default _isBlank;