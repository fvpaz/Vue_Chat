import kotlin.js.Json
import kotlin.js.json

//val users = arrayListOf<User>()

val USER_FOUND = "Ya existe un usuario con ese nombre"

data class User(val id: String, val nickname: String) {}

fun getUserList(users: ArrayList<User>): ArrayList<Json> {
    val usersList = arrayListOf<Json>()
    users.forEach {
        usersList.add(json("nickname" to it.nickname))
    }
    return usersList
}

/***
 * param:s
 */
fun ArrayList<User>.addUser(id: String, nickname: String): dynamic {
    var nickname = nickname.trim()

    val exist = find(nickname) != -1 //Search user by nickname
    return if (!exist) add(User(id, nickname)) else USER_FOUND
}

fun ArrayList<User>.removeUser(id: String): Boolean {
    val index = getUserIndex(id)
    return if (index != -1) removeAt(index) != undefined
           else false
}

fun ArrayList<User>.getUser(id: String): User {
    val index = getUserIndex(id)
    return get(index)
}

private fun ArrayList<User>.find(nickname: String): Int {
    return indexOfFirst { it.nickname == nickname }
}

private fun ArrayList<User>.getUserIndex(id: String): Int {
    return indexOfFirst { it.id == id }
}

//TODO
fun getUsersInRoom(room: String) {

}