package server

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.await
import kotlinx.coroutines.promise
import kotlin.js.Date
import kotlin.js.Json
import kotlin.js.Promise
import kotlin.js.json

external val process: dynamic
external val __dirname: dynamic

@JsModule("koa")
external class Koa
@JsModule("koa-views")
external fun views(root: String, opts: Json)

fun App() {
    val app = Koa().asDynamic()
    //val views      = require("koa-views")
    val json       = require("koa-json")
    val onerror    = require("koa-onerror")
    val bodyparser = require("koa-bodyparser")
    val logger     = require("koa-logger")
    val path       = require("path")
    val rootPath   = path.join(__dirname,"../../../")

    console.log(require("./serverapp"))
//routes
    val index = require("./serverapp").routes.index()
    val users = require("../../../routes/users")
    // error handler
    onerror(app)

// middlewares
    val bodyparserArgs =  json("enableTypes" to arrayOf("json", "form", "text"))
    val viewsArgs      = json("extension" to "pug")

    app.use(bodyparser(bodyparserArgs))
    app.use(json())
    app.use(logger())

    app.use(require("koa-static")(rootPath + "/public"))
    app.use(views(rootPath + "/views", viewsArgs))

// logger
    app.use { ctx, next ->
        GlobalScope.promise {
            val nextPromise: Promise<Any> = next()
            val start = Date.now()
            nextPromise.await()
            val ms = Date.now() - start
            console.log("${ctx.method} ${ctx.url} - ${ms}ms")
        }
    }

// routes
    app.use(index.routes(), index.allowedMethods())
    app.use(users.routes(), users.allowedMethods())

// error-handling
    app.on("error") { err, ctx ->
        console.error("server error", err, ctx)
    }

//    module.exports = app
    return app
}
