package routes

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.await
import kotlinx.coroutines.promise
import kotlin.js.Promise
import kotlin.js.json

external fun require(module: String): dynamic

fun index() {
    val ejsArgs: dynamic = json("title" to "Play in Portal")

    val router = require("koa-router")()
    router.get("/") { ctx, next ->
        GlobalScope.promise {
            val renderPromise: Promise<Any> = ctx.render("index",ejsArgs)
            renderPromise.await()
        }
    }

    return router
}