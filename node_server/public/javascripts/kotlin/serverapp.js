(function (_, Kotlin, $module$kotlinx_coroutines_core, $module$koa) {
  'use strict';
  var to = Kotlin.kotlin.to_ujzrz7$;
  var json = Kotlin.kotlin.js.json_pyyo18$;
  var coroutines = $module$kotlinx_coroutines_core.kotlinx.coroutines;
  var await_0 = $module$kotlinx_coroutines_core.kotlinx.coroutines.await_t11jrl$;
  var COROUTINE_SUSPENDED = Kotlin.kotlin.coroutines.intrinsics.COROUTINE_SUSPENDED;
  var CoroutineImpl = Kotlin.kotlin.coroutines.CoroutineImpl;
  var promise = $module$kotlinx_coroutines_core.kotlinx.coroutines.promise_pda6u4$;
  var Unit = Kotlin.kotlin.Unit;
  var Kind_CLASS = Kotlin.Kind.CLASS;
  var equals = Kotlin.equals;
  var ArrayList_init = Kotlin.kotlin.collections.ArrayList_init_287e2$;
  var throwCCE = Kotlin.throwCCE;
  var trim = Kotlin.kotlin.text.trim_gw00vp$;
  function Coroutine$index$lambda$lambda(closure$ctx_0, closure$ejsArgs_0, $receiver_0, controller, continuation_0) {
    CoroutineImpl.call(this, continuation_0);
    this.$controller = controller;
    this.exceptionState_0 = 1;
    this.local$closure$ctx = closure$ctx_0;
    this.local$closure$ejsArgs = closure$ejsArgs_0;
  }
  Coroutine$index$lambda$lambda.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: null,
    interfaces: [CoroutineImpl]
  };
  Coroutine$index$lambda$lambda.prototype = Object.create(CoroutineImpl.prototype);
  Coroutine$index$lambda$lambda.prototype.constructor = Coroutine$index$lambda$lambda;
  Coroutine$index$lambda$lambda.prototype.doResume = function () {
    do
      try {
        switch (this.state_0) {
          case 0:
            var renderPromise = this.local$closure$ctx.render('index', this.local$closure$ejsArgs);
            this.state_0 = 2;
            this.result_0 = await_0(renderPromise, this);
            if (this.result_0 === COROUTINE_SUSPENDED)
              return COROUTINE_SUSPENDED;
            continue;
          case 1:
            throw this.exception_0;
          case 2:
            return this.result_0;
          default:this.state_0 = 1;
            throw new Error('State Machine Unreachable execution');
        }
      } catch (e) {
        if (this.state_0 === 1) {
          this.exceptionState_0 = this.state_0;
          throw e;
        } else {
          this.state_0 = this.exceptionState_0;
          this.exception_0 = e;
        }
      }
     while (true);
  };
  function index$lambda$lambda(closure$ctx_0, closure$ejsArgs_0) {
    return function ($receiver_0, continuation_0, suspended) {
      var instance = new Coroutine$index$lambda$lambda(closure$ctx_0, closure$ejsArgs_0, $receiver_0, this, continuation_0);
      if (suspended)
        return instance;
      else
        return instance.doResume(null);
    };
  }
  function index$lambda(closure$ejsArgs) {
    return function (ctx, next) {
      return promise(coroutines.GlobalScope, void 0, void 0, index$lambda$lambda(ctx, closure$ejsArgs));
    };
  }
  function index() {
    var ejsArgs = json([to('title', 'Play in Portal')]);
    var router = require('koa-router')();
    router.get('/', index$lambda(ejsArgs));
    return router;
  }
  function Coroutine$App$lambda$lambda(closure$next_0, closure$ctx_0, $receiver_0, controller, continuation_0) {
    CoroutineImpl.call(this, continuation_0);
    this.$controller = controller;
    this.exceptionState_0 = 1;
    this.local$closure$next = closure$next_0;
    this.local$closure$ctx = closure$ctx_0;
    this.local$start = void 0;
  }
  Coroutine$App$lambda$lambda.$metadata$ = {
    kind: Kotlin.Kind.CLASS,
    simpleName: null,
    interfaces: [CoroutineImpl]
  };
  Coroutine$App$lambda$lambda.prototype = Object.create(CoroutineImpl.prototype);
  Coroutine$App$lambda$lambda.prototype.constructor = Coroutine$App$lambda$lambda;
  Coroutine$App$lambda$lambda.prototype.doResume = function () {
    do
      try {
        switch (this.state_0) {
          case 0:
            var nextPromise = this.local$closure$next();
            this.local$start = Date.now();
            this.state_0 = 2;
            this.result_0 = await_0(nextPromise, this);
            if (this.result_0 === COROUTINE_SUSPENDED)
              return COROUTINE_SUSPENDED;
            continue;
          case 1:
            throw this.exception_0;
          case 2:
            var ms = Date.now() - this.local$start;
            return console.log(this.local$closure$ctx.method.toString() + ' ' + this.local$closure$ctx.url.toString() + ' - ' + ms + 'ms'), Unit;
          default:this.state_0 = 1;
            throw new Error('State Machine Unreachable execution');
        }
      } catch (e) {
        if (this.state_0 === 1) {
          this.exceptionState_0 = this.state_0;
          throw e;
        } else {
          this.state_0 = this.exceptionState_0;
          this.exception_0 = e;
        }
      }
     while (true);
  };
  function App$lambda$lambda(closure$next_0, closure$ctx_0) {
    return function ($receiver_0, continuation_0, suspended) {
      var instance = new Coroutine$App$lambda$lambda(closure$next_0, closure$ctx_0, $receiver_0, this, continuation_0);
      if (suspended)
        return instance;
      else
        return instance.doResume(null);
    };
  }
  function App$lambda(ctx, next) {
    return promise(coroutines.GlobalScope, void 0, void 0, App$lambda$lambda(next, ctx));
  }
  function App$lambda_0(err, ctx) {
    console.error('server error', err, ctx);
    return Unit;
  }
  function App() {
    var app = new $module$koa();
    var views = require('koa-views');
    var json_0 = require('koa-json');
    var onerror = require('koa-onerror');
    var bodyparser = require('koa-bodyparser');
    var logger = require('koa-logger');
    var path = require('path');
    var rootPath = path.join(__dirname, '../../../');
    console.log(require('./serverapp'));
    var index = require('./serverapp').routes.index();
    var users = require('../../../routes/users');
    onerror(app);
    var bodyparserArgs = json([to('enableTypes', ['json', 'form', 'text'])]);
    var viewsArgs = json([to('extension', 'pug')]);
    app.use(bodyparser(bodyparserArgs));
    app.use(json_0());
    app.use(logger());
    app.use(require('koa-static')(rootPath + '/public'));
    app.use(views(rootPath + '/views', viewsArgs));
    app.use(App$lambda);
    app.use(index.routes(), index.allowedMethods());
    app.use(users.routes(), users.allowedMethods());
    app.on('error', App$lambda_0);
    return app;
  }
  function User(id, nickname) {
    this.id = id;
    this.nickname = nickname;
  }
  User.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'User',
    interfaces: []
  };
  User.prototype.component1 = function () {
    return this.id;
  };
  User.prototype.component2 = function () {
    return this.nickname;
  };
  User.prototype.copy_puj7f4$ = function (id, nickname) {
    return new User(id === void 0 ? this.id : id, nickname === void 0 ? this.nickname : nickname);
  };
  User.prototype.toString = function () {
    return 'User(id=' + Kotlin.toString(this.id) + (', nickname=' + Kotlin.toString(this.nickname)) + ')';
  };
  User.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.id) | 0;
    result = result * 31 + Kotlin.hashCode(this.nickname) | 0;
    return result;
  };
  User.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.id, other.id) && Kotlin.equals(this.nickname, other.nickname)))));
  };
  function main() {
  }
  var USER_FOUND;
  function User_0(id, nickname) {
    this.id = id;
    this.nickname = nickname;
  }
  User_0.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'User',
    interfaces: []
  };
  User_0.prototype.component1 = function () {
    return this.id;
  };
  User_0.prototype.component2 = function () {
    return this.nickname;
  };
  User_0.prototype.copy_puj7f4$ = function (id, nickname) {
    return new User_0(id === void 0 ? this.id : id, nickname === void 0 ? this.nickname : nickname);
  };
  User_0.prototype.toString = function () {
    return 'User(id=' + Kotlin.toString(this.id) + (', nickname=' + Kotlin.toString(this.nickname)) + ')';
  };
  User_0.prototype.hashCode = function () {
    var result = 0;
    result = result * 31 + Kotlin.hashCode(this.id) | 0;
    result = result * 31 + Kotlin.hashCode(this.nickname) | 0;
    return result;
  };
  User_0.prototype.equals = function (other) {
    return this === other || (other !== null && (typeof other === 'object' && (Object.getPrototypeOf(this) === Object.getPrototypeOf(other) && (Kotlin.equals(this.id, other.id) && Kotlin.equals(this.nickname, other.nickname)))));
  };
  function getUserList(users) {
    var usersList = ArrayList_init();
    var tmp$;
    tmp$ = users.iterator();
    while (tmp$.hasNext()) {
      var element = tmp$.next();
      usersList.add_11rb$(json([to('nickname', element.nickname)]));
    }
    return usersList;
  }
  function addUser($receiver, id, nickname) {
    var tmp$;
    var nickname_0 = trim(Kotlin.isCharSequence(tmp$ = nickname) ? tmp$ : throwCCE()).toString();
    var exist = find($receiver, nickname_0) !== -1;
    return !exist ? $receiver.add_11rb$(new User_0(id, nickname_0)) : USER_FOUND;
  }
  function removeUser($receiver, id) {
    var index = getUserIndex($receiver, id);
    return index !== -1 ? !equals($receiver.removeAt_za3lpa$(index), undefined) : false;
  }
  function getUser($receiver, id) {
    var index = getUserIndex($receiver, id);
    return $receiver.get_za3lpa$(index);
  }
  function find($receiver, nickname) {
    var indexOfFirst$result;
    indexOfFirst$break: do {
      var tmp$;
      var index = 0;
      tmp$ = $receiver.iterator();
      while (tmp$.hasNext()) {
        var item = tmp$.next();
        if (equals(item.nickname, nickname)) {
          indexOfFirst$result = index;
          break indexOfFirst$break;
        }index = index + 1 | 0;
      }
      indexOfFirst$result = -1;
    }
     while (false);
    return indexOfFirst$result;
  }
  function getUserIndex($receiver, id) {
    var indexOfFirst$result;
    indexOfFirst$break: do {
      var tmp$;
      var index = 0;
      tmp$ = $receiver.iterator();
      while (tmp$.hasNext()) {
        var item = tmp$.next();
        if (equals(item.id, id)) {
          indexOfFirst$result = index;
          break indexOfFirst$break;
        }index = index + 1 | 0;
      }
      indexOfFirst$result = -1;
    }
     while (false);
    return indexOfFirst$result;
  }
  function getUsersInRoom(room) {
  }
  var package$routes = _.routes || (_.routes = {});
  package$routes.index = index;
  var package$server = _.server || (_.server = {});
  package$server.App = App;
  package$server.User = User;
  package$server.main = main;
  Object.defineProperty(_, 'USER_FOUND', {
    get: function () {
      return USER_FOUND;
    }
  });
  _.User = User_0;
  _.getUserList_llz3iw$ = getUserList;
  _.addUser_1xat61$ = addUser;
  _.removeUser_ucqx5j$ = removeUser;
  _.getUser_ucqx5j$ = getUser;
  _.getUsersInRoom_61zpoe$ = getUsersInRoom;
  USER_FOUND = 'Ya existe un usuario con ese nombre';
  main();
  Kotlin.defineModule('serverapp', _);
  return _;
}(module.exports, require('kotlin'), require('kotlinx-coroutines-core'), require('koa')));

//# sourceMappingURL=serverapp.js.map
