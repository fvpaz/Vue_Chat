import org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile

plugins {
    id("org.jetbrains.kotlin.js") version "1.4.30-M1"
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-js"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-js:1.4.2")

    implementation(npm("debug", "^4.3.1"))
    implementation(npm("koa", "^2.13.1"))
    implementation(npm("koa-bodyparser", "^4.3.0"))
    implementation(npm("koa-hbs", "^1.0.0"))
    implementation(npm("koa-convert", "^2.0.0"))
    implementation(npm("koa-json", "^2.0.2"))
    implementation(npm("koa-logger", "^3.2.1"))
    implementation(npm("koa-onerror", "^4.1.0"))
    implementation(npm("koa-router", "^10.0.0"))
    implementation(npm("koa-static", "^5.0.0"))
    implementation(npm("koa-views", "^7.0.0"))
    implementation(npm("kotlin", "^1.4.30-M1")) //https://yarnpkg.com/package/kotlin
    implementation(npm("kotlinx-coroutines-core","^1.4.2"))
    implementation(npm("mongodb", "^3.6.3"))
    implementation(npm("socket.io", "^3.1.0"))
    //    implementation(npm("cookie-parser", "^1.4.5"))
//    implementation(npm("http-errors", "^1.7.3"))
//    implementation(npm("morgan", "^1.10.0"))
//    implementation(npm("node-sass-middleware", "0.11.0"))

}

kotlin {
    js {
        nodejs {  }
        binaries.executable()
    }
}

tasks {
    named("compileKotlinJs", Kotlin2JsCompile::class) {
        kotlinOptions.outputFile = "${projectDir}/public/javascripts/kotlin/serverapp.js"
        kotlinOptions.metaInfo = false
        kotlinOptions.moduleKind = "commonjs"
    }
}